package model;

public class MovablePoint implements Movable {
    private int x;
    private int y;

    @Override
    public void moveUp() {
        System.out.println(this.y + 1);
    }

    @Override
    public void moveDown() {
        System.out.println(this.y - 1);
    }

    @Override
    public void moveLeft() {
        System.out.println(this.x - 1);
    }

    @Override
    public void moveright() {
        System.out.println(this.x + 1);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public MovablePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "MovablePoint [x=" + x + ", y=" + y + "]";
    }

}
