import model.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(5, 7);

        point1.moveUp();
        point1.moveLeft();

        MovablePoint point2 = new MovablePoint(7, 8);
        point2.moveDown();
        point2.moveright();
    }
}
